$(document).ready(function(){


    (function(){
        //Сохраняем ссылку на стандартный метод jQuery
        var originalAddClassMethod = jQuery.fn.addClass;
        //Переопределяем
        $.fn.addClass = function(){
            var result = originalAddClassMethod.apply(this, arguments);
            //Инициализируем событие смены класса
            $(this).trigger('cssClassChanged');
            return result;
        }
    })();


    $('.qty_minus').click(function () {
        var $input = $(this).parent().find('.qty_input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? '1' : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.qty_plus').click(function () {
        var $input = $(this).parent().find('.qty_input');
        var count = parseInt($input.val()) + 1;
        $input.val(count);
        $input.change();
        return false;
    });



    $('.cart_minus').click(function () {
        var path = $(this).closest('tr').prev('tr');
        var $input = path.find('.cart_ordered');
        var inpack = path.find('td').data('inpack');
        var count = parseInt($input.text()) - 1;
        count = count < 1 ? '1' : count;
        $input.text(count);
        $input.change();

        var packs = parseInt( (count / inpack) + 1 - 0.1 );
        path.find('.cart_packs').text( packs.toFixed(0) );
        path.find('.cart_packs').change();
        return false;
    });
    $('.cart_plus').click(function () {
        var path = $(this).closest('tr').prev('tr');
        var $input = path.find('.cart_ordered');
        var inpack = path.find('td').data('inpack');
        var count = parseInt($input.text()) + 1;
        $input.text(count);
        $input.change();

        var packs = parseInt( (count / inpack) + 1 - 0.1 );
        path.find('.cart_packs').text( packs.toFixed(0) );
        path.find('.cart_packs').change();
        return false;
    });


    $('.cart_m_minus').click(function() {
        var path = $(this).parent();
        var $input = path.find('.cart_count_mobile');
        var inpack = path.data('inpack');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? '1' : count;
        $input.val(count);
        $input.change();

        var packs = parseInt( (count / inpack) + 1 - 0.1 );
        $(this).closest('.cart_row_mobile').find('.mobile_packs').text( packs.toFixed(0) );
        $(this).closest('.cart_row_mobile').find('.mobile_packs').change();
        return false;
    });
    $('.cart_m_plus').click(function () {
        var path = $(this).parent();
        var $input = path.find('.cart_count_mobile');
        var inpack = path.data('inpack');
        var count = parseInt($input.val()) + 1;
        $input.val(count);
        $input.change();

        var packs = parseInt( (count / inpack) + 1 - 0.1 );
        $(this).closest('.cart_row_mobile').find('.mobile_packs').text( packs.toFixed(0) );
        $(this).closest('.cart_row_mobile').find('.mobile_packs').change();
        return false;
    });

    // СЛЕДИЛКИ ЗА ИЗМЕНЕНИМ КОЛ-ВА В КОРЗИНЕ В PC и MOBILE ВЕРСИЯХ (синхронизатор значений)
    $('.cart_ordered').change(function(){
      var newVal = $(this).text();
      var mobileItem = $(this).closest('tr').next().next().next('.cart_tr_mobile');
      mobileItem.find('.cart_count_mobile').val(newVal);
    });

    $('.cart_packs').change(function(){
      var mobileItem = $(this).closest('tr').next().next().next('.cart_tr_mobile');
      var newPack = $(this).text();
      mobileItem.find('.mobile_packs').text(newPack);
    });


    $('.cart_count_mobile').change(function(){
      var newVal = $(this).val();
      var pcItem = $(this).closest('.cart_tr_mobile').prev().prev().prev('.oneline');
      pcItem.find('.cart_ordered').text(newVal);
    });

    $('.mobile_packs').change(function(){
      var pcItem = $(this).closest('.cart_tr_mobile').prev().prev().prev('.oneline');
      var newPack = $(this).text();
      pcItem.find('.cart_packs').text(newPack);
    });


    (function(){
        //Сохраняем ссылку на стандартный метод jQuery
        var originalAddClassMethod = jQuery.fn.addClass;
        //Переопределяем
        $.fn.addClass = function(){
            var result = originalAddClassMethod.apply(this, arguments);
            //Инициализируем событие смены класса
            $(this).trigger('cssClassChanged');
            return result;
        }
    })();


    // INDEX SLIDER
    $('.index_slider').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        dots: true,
        mouseDrag: false,
        autoplay: true,
        autoplayTimeout: 7000,
        animateOut: 'zoomOut',
        animateIn: 'fadeIn'
    });

    $('.index_slider').each(function(){
      var dots = $(this).find('.owl-dot');
      var margin = ((dots.outerWidth() * dots.length) / 2) + 10;
      $(this).find('.owl-prev').css('margin-left', -margin-30 );
      $(this).find('.owl-next').css('margin-left', margin );
    });


    // MOBILE SIMILAR PRODUCTS
    $('.similar_list').owlCarousel({
          items: 1,
          loop: false,
          nav: true,
          dots: true,
          mouseDrag: false,
          responsive: {
            0: {
              items: 1
            },
            440: {
              items: 2
            },
            660: {
              items: 3
            },
            800: {
              items: 4
            },
            900: {
              items: 5
            }
          }
    });

    var similarSlides = $('.similar_list .owl-dot').length;
    $('<div class="similar_counter"><span>1/</span> '+ similarSlides +'</div>').insertAfter('.similar_list .owl-nav');
    $(function(){
        $('.similar_list .owl-dot').bind('cssClassChanged', function(){ 
            var index = $(this).parent().find('.active').index() + 1;
            $('.similar_counter span').text(index+'/');
        });
    });

    function mobileSliders() {

      if ( $(window).width() <= 660 ) {
        $('.partners_list').addClass('owl-carousel');
        $('.partners_list').owlCarousel({
              items: 2,
              loop: true,
              nav: false,
              dots: true,
              mouseDrag: false,
              responsive: {
                0: {
                  items: 1
                },
                340: {
                  items: 2
                }
              }
        });
      } else {
        $('.partners_list').removeClass('owl-carousel');
      }
    };
    mobileSliders();


    $('.product_imgs_box_mobile').owlCarousel({
          items: 2,
          loop: true,
          nav: true,
          dots: true,
          mouseDrag: false,
          responsive: {
            0: {
              items: 1
            },
            480: {
              items: 3,
              margin: 20
            },
            660: {
              items: 1
            }
          }
    });



    // SCROLL TO TOP
    $('#toTop').click(function(e) {
        e.preventDefault();
        $('body, html').animate({
            scrollTop : 0
        }, 1200);
    });


    // ПРИКЛЕЙКА МЕНЮ К ВЕРХУ ПРИ СКРОЛЛЕ
    var stickyNavTop = $('.topnav_sec_skelet').offset().top;
    var stickyNav = function(){
        var scrollTop = $(window).scrollTop();
              
        if (scrollTop > stickyNavTop ) { 
            $('.topnav_sec').addClass('fixed'); 
        } else {
            $('.topnav_sec').removeClass('fixed'); 
        }
    };
     
    stickyNav();
     
    $(window).scroll(function() {
        stickyNav();
    });


    // Replace all SVG images with inline SVG
    $('img.svg').each(function(){
      var $img = $(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src');

      $.get(imgURL, function(data) {
          // Get the SVG tag, ignore the rest
          var $svg = $(data).find('svg');

          // Add replaced image's ID to the new SVG
          if(typeof imgID !== 'undefined') {
              $svg = $svg.attr('id', imgID);
          }
          // Add replaced image's classes to the new SVG
          if(typeof imgClass !== 'undefined') {
              $svg = $svg.attr('class', imgClass+' replaced-svg');
          }

          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');

          // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
          if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
              $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
          }

          // Replace image with new SVG
          $img.replaceWith($svg);

      }, 'xml');

    });


    //  MOBILE MENU BUTTON
    $("#menu").mmenu({
       "extensions": [
          "pagedim-black",
          "theme-dark"
       ],
       "offCanvas": {
          "position": "left"
       },  
       "counters": true,
       "navbars": [
          {
             "position": "bottom",
             "content": [
                "<a class='fa fa-facebook' href='#'></a>",
                "<a class='fa fa-instagram' href='#'></a>",
                "<a class='fa fa-youtube' href='#'></a>"
             ]
          }
       ]
    });

    // CHECKBOXES / RADIO
    $('.ch + label, .rd + label, .filter_check + label').click(function(){
      $(this).prev('input').click();
    });

    // TABS
    $('.tabs_container div.tab:not(:first)').hide();
    $('.tabs_nav li').click(function(event) {
        event.preventDefault();
        $('.tabs_container div.tab').hide();
        $('.tabs_nav .current').removeClass("current");
        $(this).addClass('current');

        var clicked = $(this).find('a:first').attr('href');
        $('.tabs_container ' + clicked).fadeIn('fast');
    }).eq(0).addClass('current');

    // FULL SCREEN MIN-HEIGHT
    function fullHeight() {
      $('.full-height').css('min-height', $(window).height() );
    }
    fullHeight();

    $(window).resize(function(){
      fullHeight();
    });

    $('.cols-2').each(function(){
      var count = $(this).children().length;
      $(this).children().last().addClass('last');
      $(this).children('*:nth-child('+count/2+')').addClass('last');
    });


    $('.vakansia_item h4').click(function(){
      var item = $(this).closest('.vakansia_item');
      item.toggleClass('opened');
      if ( item.hasClass('opened') ) {
        item.find('.vakansia_desc').slideDown();
      } else {
        item.find('.vakansia_desc').slideUp();
      }
      return false;
    });

    $('.vakansia_item').each(function(){
      if ( $(this).hasClass('opened') ) {
        $(this).find('.vakansia_desc').show();
      } else {
        $(this).find('.vakansia_desc').hide();
      }
      return false;
    });

    // MIN HEIGHT FOR PRODUCTS IN CATEGORY
    function productMinHeight() {
      setTimeout(function() {
        var productHeight = 0;
        $('.product_item').css('min-height', '10px');
        $('.product_item').each(function(){
          if ($(this).outerHeight() > productHeight ) {
            productHeight = $(this).outerHeight();
            $('.product_item').css('min-height', productHeight);
          }
        });
      }, 1000);
    }

    function collectionMinHeight() {
      setTimeout(function() {
        var collectionHeight = 0;
        $('.collection_item').css('min-height', '10px');
        $('.collection_item').each(function(){
          if ($(this).outerHeight() > collectionHeight ) {
            collectionHeight = $(this).outerHeight();
            $('.collection_item').css('min-height', collectionHeight);
          }
        });
      }, 1000);
    }

    collectionMinHeight();
    productMinHeight();
    $(window).resize(function(){
        productMinHeight();
        collectionMinHeight();
    });

    $('.open_fast_preview').click(function(){
      $('.fast_preview_box').fadeIn();
      $('.slick-next').click();
      setTimeout(function() { $('.slick-prev').click(); }, 300);
      return false;
    });

    $('.fast_preview_close').click(function(){
      $('.fast_preview_box').fadeOut();
      return false;
    });


    // CART ROW OPEN
    $('.cart_item_box h2').click(function(){
      var thisContent = $(this).parent().find('.cart_item_content')
      if ( $(this).parent().hasClass('opened') ) {
        $('.cart_item_box').removeClass('opened');
      } else {
        $('.cart_item_box').removeClass('opened');
        $(this).parent().addClass('opened');
        $('body, html').animate({scrollTop: $(this).offset().top }, 300);
      }
    });

    // DELIVERY / PICKUP CHANGE
    $('#delivery_radio').click(function(){
      $('#delivery_box').fadeIn(500);
    });
    $('#pickup_radio').click(function(){
      $('#delivery_box').fadeOut(500);
    });


    $('.open_order').click(function(){
      $('.history_item').removeClass('opened');
      $(this).closest('.history_item').addClass('opened');
      return false;
    });

    $('.details_close').click(function(){
      $('.history_item').removeClass('opened');
      return false;
    });

    $('.product_thumbs_vertical_slider').slick({
      vertical: true,
      verticalSwiping: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      appendArrows: $('.vslider_arrows'),
      speed: 200,
      responsive: [
      {
        breakpoint: 1460,
        settings: {
          vertical: true,
          verticalSwiping: true,
          slidesToShow: 2,
          slidesToScroll: 2          
        }
      },
      {
        breakpoint: 800,
        settings: {
          vertical: false,
          verticalSwiping: false
        }
      },
      {
        breakpoint: 660,
        settings: {
          vertical: false,
          verticalSwiping: false,
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 578,
        settings: {
          vertical: true,
          verticalSwiping: true,
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 440,
        settings: {
          vertical: false,
          verticalSwiping: false,
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      ]
    });

    $('.fast_vertical_slider').slick({
      vertical: true,
      verticalSwiping: true,
      slidesToShow: 6,
      slidesToScroll: 6,
      infinite: true,
      speed: 200,
      responsive: [
      {
        breakpoint: 1460,
        settings: {
          vertical: false,
          verticalSwiping: false,
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      ]
    });

    $('.fast_slider_horizontal').slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      infinite: true,
      speed: 200,
      responsive: [
      {
        breakpoint: 1024,
        settings: {
          vertical: false,
          verticalSwiping: false,
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      ]
    });

    $('.order_vslider').slick({
      vertical: true,
      verticalSwiping: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: false,
      speed: 600,
      responsive: [
      {
        breakpoint: 660,
        settings: {
          vertical: false,
          verticalSwiping: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      ]
    });

    $('.pairedThumbs .slick-slide:first-child a').addClass('active');
    $('.pairedThumbs a').click(function(){
      var main = $(this).closest('.pairedImgsBox').find('.mainPaired');
      var href = $(this).attr('href');
      main.attr('href', href).children('img').attr('src', href);
      $(this).closest('.pairedImgsBox').find('a').removeClass('active');
      $(this).addClass('active');
      return false;
    });


    $('.open_filters_mobile a').click(function(){
      $('.nav_column').fadeIn(300);
      return false;
    });

    $('.close_navbar').click(function(){
      $('.nav_column').fadeOut(300);
      return false;
    });

    $(window).resize(function(){
      if ( $(window).width() > 660 ) {
        $('.nav_column').show();
      } else {
        $('.nav_column').hide();
      }
    });


    // MOBILE SEARCH FORM
    $('.open_search_mobile').click(function(){
      $('.top_search_li form').fadeIn(300);
      return false;
    });
    $('.close_search_mobile').click(function(){
      $('.top_search_li form').fadeOut(300);
      return false;
    });


    // ACCORDION
    $('.product_accordion_item.opened').find('.product_accordion_content').show();
    $('.product_accordion_item h3').click(function(){
      if ( $(this).parent().hasClass('opened') ) {
        $(this).parent().removeClass('opened');
        $(this).next('.product_accordion_content').slideUp(300);
      } else {
        $(this).parent().addClass('opened');
        $(this).next('.product_accordion_content').slideDown(300);
      }
      return false;
    });



    $('.lichniy_mobile_h3.opened').next('.tab').show();
    $('.lichniy_mobile_h3').click(function(){
      if ( $(this).hasClass('opened') ) {
        $('.lichniy_mobile_h3').removeClass('opened').next('.tab').fadeOut(300);
        $(this).next('.tab').fadeOut(300);
      } else {
        $('.lichniy_mobile_h3').removeClass('opened').next('.tab').fadeOut(300);
        $(this).addClass('opened');
        $(this).next('.tab').fadeIn(300);
        $('html, body').scrollTop( $(this).offset().top );
      }
      return false;
    });

    $(window).resize(function(){
      if ( $('.lichniy_tabs_mobile').length ) {
          if ( $(window).width() > 660 ) {
            $('.lichniy_tabs_mobile .tab').hide();
            $('.lichniy_tabs_mobile h3:first-child + .tab').show();
            $('.tabs_nav li:first-child').addClass('current');
          } else {
            $('.lichniy_tabs_mobile .tab').hide();
            $('.lichniy_mobile_h3').removeClass('opened');
          }
      }
    });


    function catalogMinHeight() {
      setTimeout(function() {
        var catalogHeight = 0;
        $('.catalog_item_title').each(function(){
          if ($(this).outerHeight() > catalogHeight ) {
            catalogHeight = $(this).outerHeight();
            $('.catalog_item_title').css('min-height', catalogHeight);
          }
        });
      }, 100);
    }

    catalogMinHeight();

    $(window).resize(function(){
      catalogMinHeight();
    });


});

